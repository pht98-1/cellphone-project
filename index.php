<?php
include "route.php";

$route = new Route();

$route->add('/', function () {

    include_once './views/Pages/home.php';
});

$route->add('/login', function () {
    include_once './views/Pages/login.php';
    include "function.php";

    //khởi tạo 1 session mới
    session_start();
    $username = $_POST['username'];
    $password = $_POST['password'];
    $token = token_password($password); //dùng để mã hóa mật khẩu khi nhập vào form 
    //tạo câu lện SQL để truy vấn và tìm kiếm dữ liệu từ database
    if (isset($_POST["login-form"])) {
        $sql = "SELECT * FROM `user` WHERE username = '$username' AND password = '$token'";
        //thực thi câu lệnh SQL và lưu kết quả vào biết $result
        $result = $db_connection->query($sql);

        //lưu dữ liệu của object này vào biến $row(array)
        $row = mysqli_fetch_array($result);

        //Check xem có dữ liệu trong database khớp với người dùng nhập vào hay không
        if (is_array($row)) {
            //lưu thông tin của username & password vào 2 biến SESSION
            $_SESSION['userType'] = $row[3];
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
?>
            <script>
                alert("Login Succeed !");
                window.location.href = "index.php";
            </script>
        <?php } else { ?>
            <script>
                alert("Login Failed !");
                window.location.href = "";
            </script>
    <?php }
    }
    //bỏ qua form login nếu người dùng đã login thành công
    if (isset($_SESSION['username']) && isset($_SESSION['password'])) {
        header("Location: index.php");
    }
});

$route->add('/logout', function () {
    session_start();
    $_SESSION = array();
    unset($_SESSION); //dùng để bỏ Session 
    session_destroy();

    header("Location: index.php");
    exit;
});

$route->add('/delete_product', function () {
    require_once "function.php";
    $id = $_POST['id'];
    $sql = "DELETE FROM product WHERE Product_ID = '$id'";
    $result =  $db_connection->query($sql);
    ?>
    <script>
        alert("Delete Student Succeed !");
        window.location.href = "index.php";
    </script>
<?php
});

$route->add('/add_product', function () {
    
    require_once "function.php";
    include_once './views/Partials/addProduct.php';
    
    
if (isset($_POST['add'])) {
    $name = $_POST['name'];
    $info = $_POST['info'];
    $year = $_POST['year'];
    $price = $_POST['price'];
    $qty = $_POST['qty'];
    $category = $_POST['category'];
    $image = "";
    //đoạn code dùng để upload & xử lý ảnh
    //kiểm tra người dùng đã chọn file ảnh có dung lượng khác 0
    if (isset($_FILES['image']) && $_FILES['image']['size'] != 0) {
        //khai báo biến dùng để lưu file ảnh vào đường dẫn tạm thời
        $temp_name = $_FILES['image']['tmp_name'];
        //khai báo biến dùng để lưu tên của ảnh
        $img_name = $_FILES['image']['name'];
        //tách tên file ảnh dựa vào dấu chấm
        $parts = explode(".", $img_name);
        //tìm index cuối cùng
        $lastIndex = count($parts) - 1;
        //lấy ra extension (đuôi) file ảnh
        $extension = $parts[$lastIndex];
        //thiết lập tên mới cho ảnh
        $image = $name . "_" . $category . "." . $extension;
        //thiết lập địa chỉ file ảnh cần di chuyển đến
        $image_folder = "images/phones/";
        $destination = $image_folder . $image;
        //di chuyển file ảnh từ đường dẫn tạm thời đến địa chỉ đã thiết lập
        move_uploaded_file($temp_name, $destination);
    }

    //Kiểm tra xem class đã full sinh viên hay chưa




        $sql1 = "INSERT INTO product (Product_Name, Product_Information, Product_Year, Product_Price, Product_quantities, Product_Category, Product_Images)
        VALUES ('$name', '$info', '$year', '$price', '$qty', '$category', '$image')";
    $run1 = $db_connection->query($sql1);
    if ($run1) { ?>
        <script>
            alert("Add product successfully !");
            window.location.href = "index.php";
        </script>
    <?php } else { 
        echo $db_connection->error;
         }
}
});

$route->add('/update_product', function(){

    require_once "function.php";
    include_once './views/Partials/updateProduct.php';

    if (isset($_POST['update'])) {
        $name = $_POST['name'];
        $info = $_POST['info'];
        $year = $_POST['year'];
        $price = $_POST['price'];
        $qty = $_POST['qty'];
        $category = $_POST['category'];
        $image = "";
    //đoạn code dùng để upload & xử lý ảnh
    //kiểm tra người dùng đã chọn file ảnh có dung lượng khác 0
    if (isset($_FILES['image']) && $_FILES['image']['size'] != 0) {	
       //khai báo biến dùng để lưu file ảnh vào đường dẫn tạm thời
        $temp_name = $_FILES['image']['tmp_name'];
        //khai báo biến dùng để lưu tên của ảnh
        $img_name = $_FILES['image']['name'];
        //tách tên file ảnh dựa vào dấu chấm
        $parts = explode(".", $img_name);
        //lấy ra extension (đuôi) file ảnh
        $extension = end($parts);
        //thiết lập tên mới cho ảnh
        $image = $name . "_" . $category . "." . $extension;
        //thiết lập địa chỉ file ảnh cần di chuyển đến
        $image_folder = "images/phones/";
        $destination = $image_folder . $image;
        //di chuyển file ảnh từ đường dẫn tạm thời đến địa chỉ đã thiết lập
        move_uploaded_file($temp_name, $destination);
    } 
    else { //người dùng không update ảnh => lấy lại ảnh cũ
        $image =  $row['Product_Images'];
    }
    $query12 = "UPDATE product SET product_name = '$name', product_information = '$info',product_year = '$year', product_price = '$price', 
              product_quantities = '$qty', product_category = '$category', product_images = '$image' 
              WHERE product_id = '$id'";
    $result12 = $db_connection->query($query12);
    if ($result12) { ?>
      <script>
          alert("Update successfully !");
          window.location.href = "index.php";
      </script>
    <?php } else { 

          echo $db_connection->error;
     
     } }
});


$route->submit();
