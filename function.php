<?php
//khai báo các tham số để kết nối đến database(MySQL)
$database_name = "cellphone";
$host_name = "localhost";
$db_username = "root";
$db_password = "root";
$db_port = "3306";
//tạo kết nối đến database
$db_connection = new mysqli(
    $host_name,
    $db_username,
    $db_password,
    $database_name,
    $db_port
);

//Tạo function để chạy câu lệnh SQL
function execute_query($sql)
{
    global $db_connection;
    return $db_connection->query($sql);
}
//Tạo function để mã hóa mật khẩu
function token_password($password)
{
    return md5($password);
}
