<?php
//mục đích của file là kiểm tra người dùng 
//đã login hay chưa
//nếu chưa login thì tự động redirect về trang login

session_start();

if (!isset($_SESSION['username']) && !isset($_SESSION['password'])) { ?>
    <script>
        alert("You must LOGIN FIRST !!");
        window.location.href = "index.php";
    </script>
<?php } ?>