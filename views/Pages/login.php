<form method="POST">
    <!-- Nếu để action ="" tức là gửi và nhận dữ liệu trong cùng 1 file duy nhất -->
    <fieldset>
        <legend>User Login</legend>
        <input type="text" placeholder="Username" required name="username">
        <br> <br>
        <input type="password" placeholder="Password" required name="password">
        <br> <br>
        <input type="submit" value="Login" name="login-form">
    </fieldset>
</form>