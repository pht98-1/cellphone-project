<?php
session_start();
include_once './views/partials/header.php';
if (isset($_SESSION['userType']) && $_SESSION['userType'] == "ADMIN") {
    include_once './views/partials/adminPage.php';
}
