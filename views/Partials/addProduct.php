<form style="width: fit-content; margin-top: 30px;" action="" method="POST" enctype="multipart/form-data">
        <!-- Lưu ý: bổ sung thuộc tính enctype vào form khi upload file -->
        <fieldset>
            <legend>Add Product </legend>
            Name: <input type="text" name="name" required maxlength="50"> <br> <br>
            Info: <textarea name="info">Information</textarea> <br><br>
            Price: <input type="number" name="price" required maxlength="100000000"> <br> <br>
            Release date: <input type="date" name="year" required> <br> <br>
            Current quantity: <input type="number" name="qty" required maxlength="100000"> <br> <br>
            Category: <br> <br>
            <?php
            $sql = "SELECT * FROM category";
            $run = $db_connection->query($sql); ?>
            <select name="category">
                <?php
                while ($row = mysqli_fetch_array($run)) { ?>
                    <option value='<?= $row['Category_ID'] ?>'> <?= $row['Category_Name'] ?> </option>
                <?php } ?>
            </select>
            <br> <br>
            Image: <input type="file" name="image"> <br> <br>
            <input type="submit" value="Add" name="add">
        </fieldset>
    </form>
