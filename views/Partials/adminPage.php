<?php
require_once "function.php";
//luôn nhớ gõ đúng theo những gì mình đã đặt trong database mysql
$sql = "SELECT p.*, c.Category_Name FROM product as p
inner join category as c where p.product_category = c.category_id";
$result = $db_connection->query($sql);
?>
<table border="5">
    <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Product Quantities</th>
        <th>Product Year</th>
        <th>Product Information</th>
        <th>Product Price</th>
        <th>Product Images</th>
        <th>Product Category</th>
        <th>Menu</th>
    </tr>
    <?php while ($row = mysqli_fetch_array($result)) { ?>
        <tr>
            <td><?= $row[0] ?></td>
            <td><?= $row[1] ?></td>
            <td><?= $row[2] ?></td>
            <td><?= $row[3] ?></td>
            <td><?= $row[4] ?></td>
            <td><?= $row[6] ?></td>
            <td><img src="images/phones\<?= $row['Product_Images'] ?>" width="200" height="200"></td>
            <td><?= $row[8] ?></td>


            <td>
                <form action="update_product" method="POST">
                    <input type="hidden" name="id" value="<?= $row[0] ?>">
                    <input type="submit" value="UPDATE">
                </form>
                <form action="delete_product" method="POST" onclick="return confirm_delete();">
                    <input type="hidden" name="id" value="<?= $row[0] ?>">
                    <input type="submit" value="DELETE" name="delete">

                </form>
            </td>
        </tr>

    <?php } ?>
</table>
<a href="add_product">Create new</a>
<!--tạo code để cho người dùng xác nhận có muốn xóa dữ liệu hay không-->
<script>
    function confirm_delete() {
        var del = confirm("Do you want to delete this product ?");
        if (del) {
            return true;
        } else {
            return false;
        }
    }
</script>